provider "aws" {
  region  = "us-east-1"
  profile = "default" ## project profile
}

## Mongo DB Server ##
module "server1" {
  source           = "./module/singlebuild"
  count            = 2
  instance_name    = "PANVLDISDB0${count.index}"
  InstanceType     = var.instancetype[0] ### instance type
  root-size        = var.ec2_root_size   ### root volume size
  subnetid         = "${lookup(var.subnetid, count.index)}"
  ami-id           = var.amiid["ubuntu"] ## ami id
  security-groups  = data.terraform_remote_state.sg.outputs.Managed-services-sg
  keyid            = var.keyid          ### Keyname
  account_id       = var.accountid      ## Account number
  KMSKEYID         = var.kmskeyid       ## encryption key arn
  Billingcode      = var.billingcode    #FPE03521-SG-SC-DI-3001
  mrp              = var.mrp            #myself
  environment      = var.environment    #make it var
  requestid        = var.requestid      #chg
  billingcontact   = var.billingcontact #primary contact
  client           = var.client         #disoc
  contact          = var.contact        #ppmd
  country          = var.country        #usa
  csclass          = var.csclass
  csqual           = var.csqual
  cstype           = var.cstype
  function         = var.function
  groupcontact     = var.groupcontact
  primarycontact   = var.primarycontact   #kenny emailid
  secondarycontact = var.secondarycontact #prag
  role             = "DB"                 ### Specify the Role of the server
  memberfirm       = var.memberfirm
}


## UI Path Server ##
module "windows_ui" {
  source           = "./module/singlebuild"
  count            = 2
  instance_name    = "PANVWDISAP0${count.index}"
  InstanceType     = var.instancetype[2] ### instance type
  root-size        = var.ec2_root_size   ### root volume size
  subnetid         = "${lookup(var.subnetid, count.index)}"
  ami-id           = var.amiid["windows"] ## ami id
  security-groups  = data.terraform_remote_state.sg.outputs.Managed-services-sg
  keyid            = var.keyid          ### Keyname
  account_id       = var.accountid      ## Account number
  KMSKEYID         = var.kmskeyid       ## encryption key arn
  Billingcode      = var.billingcode    #FPE03521-SG-SC-DI-3001
  mrp              = var.mrp            #myself
  environment      = var.environment    #make it var
  requestid        = var.requestid      #chg
  billingcontact   = var.billingcontact #primary contact
  client           = var.client         #disoc
  contact          = var.contact        #ppmd
  country          = var.country        #usa
  csclass          = var.csclass
  csqual           = var.csqual
  cstype           = var.cstype
  function         = var.function
  groupcontact     = var.groupcontact
  primarycontact   = var.primarycontact   #kenny emailid
  secondarycontact = var.secondarycontact #prag
  role             = "APP"                 ### Specify the Role of the server
  memberfirm       = var.memberfirm
}

## RedHat Application Servers (Sailpoint+ForgeRock) ##
module "redhat" {
  source           = "./module/singlebuild"
  count            = 4
  instance_name    = "PANVLDISAP0${count.index + 3}"
  InstanceType     = var.instancetype[1] ### instance type
  root-size        = var.ec2_root_size   ### root volume size
  #subnetid         = "${lookup(var.subnetid, count.index)}"
  subnetid         = var.subnet_id[count.index % length(var.subnet_id)]
  ami-id           = var.amiid["redhat"] ## ami id
  security-groups  = data.terraform_remote_state.sg.outputs.Managed-services-sg
  keyid            = var.keyid          ### Keyname
  account_id       = var.accountid      ## Account number
  KMSKEYID         = var.kmskeyid       ## encryption key arn
  Billingcode      = var.billingcode    #FPE03521-SG-SC-DI-3001
  mrp              = var.mrp            #myself
  environment      = var.environment    #make it var
  requestid        = var.requestid      #chg
  billingcontact   = var.billingcontact #primary contact
  client           = var.client         #disoc
  contact          = var.contact        #ppmd
  country          = var.country        #usa
  csclass          = var.csclass
  csqual           = var.csqual
  cstype           = var.cstype
  function         = var.function
  groupcontact     = var.groupcontact
  primarycontact   = var.primarycontact   #kenny emailid
  secondarycontact = var.secondarycontact #prag
  role             = "APP"                 ### Specify the Role of the server
  memberfirm       = var.memberfirm
}


## Ubuntu Application Servers (DevOps + OnBoarding) ##
module "ubuntu-ap" {
  source           = "./module/singlebuild"
  count            = 4
  instance_name    = "PANVLDISAP0${count.index + 7}"
  InstanceType     = var.instancetype[1] ### instance type
  root-size        = var.ec2_root_size   ### root volume size
  subnetid         = var.subnet_id[count.index % length(var.subnet_id)]
  ami-id           = var.amiid["ubuntu"] ## ami id
  security-groups  = data.terraform_remote_state.sg.outputs.Managed-services-sg
  keyid            = var.keyid          ### Keyname
  account_id       = var.accountid      ## Account number
  KMSKEYID         = var.kmskeyid       ## encryption key arn
  Billingcode      = var.billingcode    #FPE03521-SG-SC-DI-3001
  mrp              = var.mrp            #myself
  environment      = var.environment    #make it var
  requestid        = var.requestid      #chg
  billingcontact   = var.billingcontact #primary contact
  client           = var.client         #disoc
  contact          = var.contact        #ppmd
  country          = var.country        #usa
  csclass          = var.csclass
  csqual           = var.csqual
  cstype           = var.cstype
  function         = var.function
  groupcontact     = var.groupcontact
  primarycontact   = var.primarycontact   #kenny emailid
  secondarycontact = var.secondarycontact #prag
  role             = "APP"                 ### Specify the Role of the server
  memberfirm       = var.memberfirm
}


## windows application servers (LDAP + Jump Server) ##
module "windows_app" {
  source           = "./module/singlebuild"
  count            = 4
  instance_name    = "PANVWDISAP${count.index + 11}"
  InstanceType     = var.instancetype[0] ### instance type
  root-size        = var.ec2_root_size   ### root volume size
  subnetid         = var.subnet_id[count.index % length(var.subnet_id)]
  ami-id           = var.amiid["windows"] ## ami id
  security-groups  = data.terraform_remote_state.sg.outputs.Managed-services-sg
  keyid            = var.keyid          ### Keyname
  account_id       = var.accountid      ## Account number
  KMSKEYID         = var.kmskeyid       ## encryption key arn
  Billingcode      = var.billingcode    #FPE03521-SG-SC-DI-3001
  mrp              = var.mrp            #myself
  environment      = var.environment    #make it var
  requestid        = var.requestid      #chg
  billingcontact   = var.billingcontact #primary contact
  client           = var.client         #disoc
  contact          = var.contact        #ppmd
  country          = var.country        #usa
  csclass          = var.csclass
  csqual           = var.csqual
  cstype           = var.cstype
  function         = var.function
  groupcontact     = var.groupcontact
  primarycontact   = var.primarycontact   #kenny emailid
  secondarycontact = var.secondarycontact #prag
  role             = "APP"                 ### Specify the Role of the server
  memberfirm       = var.memberfirm
}
