data "terraform_remote_state" "vpc" {
  backend = "local"

  config = {
    path = "../statefiles/vpc.tfstate"
  }
}

data "terraform_remote_state" "sg" {
  backend = "local"

  config = {
    path = "../statefiles/sg.tfstate"
  }
}

