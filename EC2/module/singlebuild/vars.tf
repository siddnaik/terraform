variable "ami-id" {
  type = "string"
}

variable "InstanceType" {
  type = "string"
}

variable "instance_name" {
  type = "string"
}

variable "account_id" {
  type = "string"
}

variable "keyid" {
  type = "string"
}

variable "subnetid" {
  type = "string"
}

variable "security-groups" {
  type = "string"
}

variable "root-size" {
  type = "string"
}

variable "KMSKEYID" {
  type = "string"
}

variable "Billingcode" {
  type = "string"
}

variable "mrp"{
  type = "string"
}

variable "environment" {
  type = "string"
}

variable "requestid"{
  type = "string"
}

variable "billingcontact"{
  type = "string"
}

variable "contact"{
  type = "string"
}

variable "client" {
  type = "string"
}

variable "country" {
  default = "US"
  type = "string"
}

variable "csclass"{
  default = "Confidential"
  type = "string"
}

variable "csqual"{
  default = "Confidential"
  type = "string"
}

variable "cstype"{
  default = "Confidential"
  type = "string"
}

variable "function"{
  default = "Con"
  type = "string"
}

variable "groupcontact"{
  default = "Con"
  type = "string"
}

variable "primarycontact"{
  default = "Con"
  type = "string"
}

variable "secondarycontact"{
  default = "Con"
  type = "string"
}

variable "role"{
  default = "Con"
  type = "string"
}

variable "memberfirm"{
  default = "US"
  type = "string"
}
