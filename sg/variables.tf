variable "giver" {
  type = map(string)
  default = {
    profile = "default"
  }
}

#AWS Region for launching the resource

variable "region" {
  description = "Region to lunch the resource"
  default     = "us-east-1"
}
